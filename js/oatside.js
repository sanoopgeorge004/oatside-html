$(window).on('load', function () {
  $('#status').fadeOut();
  $('#preloader').delay(350).fadeOut('slow');
  $('body').delay(350).css({ 'overflow': 'auto' });
})

$('.menu-toggle').on('click', function () {
  if ($('body').hasClass('topMenuOpen')) {
    $('header').css({ 'transform': 'translateY(0px)' });
    $('.clickMe').trigger('click');
  }
  $('body').toggleClass('open');

});



$('#instagram').flickity({
  // options
  prevNextButtons: false,
  pageDots: false,
  wrapAround: true,
  autoPlay: true,
  pauseAutoPlayOnHover: false,
  cellAlign: 'center'
});
$('.menu-dropdown > a').click(function (e) {
  e.preventDefault();
  $(this).parent('.menu-dropdown').find('.submenu').slideToggle();
  $(this).parent('.menu-dropdown').toggleClass('menuOpen');
});



function peopleSaying() {
  let tickerSpeed = 0.5;
  let flickity = null;
  let isPaused = false;
  const slideshowEl = document.querySelector('#people-saying');
  const update = () => {
    if (isPaused) return;
    if (flickity.slides) {
      flickity.x = (flickity.x - tickerSpeed) % flickity.slideableWidth;
      flickity.selectedIndex = flickity.dragEndRestingSelect();
      flickity.updateSelectedSlide();
      flickity.settle(flickity.x);
    }
    window.requestAnimationFrame(update);
  };
  const pause = () => {
    isPaused = true;
  };
  const play = () => {
    if (isPaused) {
      isPaused = false;
      window.requestAnimationFrame(update);
    }
  };

  flickity = new Flickity(slideshowEl, {
    autoPlay: false,
    prevNextButtons: false,
    pageDots: false,
    draggable: true,
    wrapAround: true,
    selectedAttraction: 0.015,
    friction: 0.25,
    imagesLoaded: true,
    adaptiveHeight: false
  });
  flickity.x = 0;
  slideshowEl.addEventListener('mouseenter', pause, false);
  slideshowEl.addEventListener('focusin', pause, false);
  slideshowEl.addEventListener('mouseleave', play, false);
  slideshowEl.addEventListener('focusout', play, false);

  flickity.on('dragStart', () => {
    isPaused = true;
  });
  update();

}

function heroSlider() {
  let tickerSpeed = 0.5;
  let flickity = null;
  let isPaused = false;
  const slideshowEl = document.querySelector('#our-heors');
  const update = () => {
    if (isPaused) return;
    if (flickity.slides) {
      flickity.x = (flickity.x - tickerSpeed) % flickity.slideableWidth;
      flickity.selectedIndex = flickity.dragEndRestingSelect();
      flickity.updateSelectedSlide();
      flickity.settle(flickity.x);
    }
    window.requestAnimationFrame(update);
  };
  const pause = () => {
    isPaused = true;
  };
  const play = () => {
    if (isPaused) {
      isPaused = false;
      window.requestAnimationFrame(update);
    }
  };

  flickity = new Flickity(slideshowEl, {
    autoPlay: false,
    prevNextButtons: false,
    pageDots: false,
    draggable: true,
    wrapAround: true,
    selectedAttraction: 0.015,
    friction: 0.25,
    imagesLoaded: true
  });
  flickity.x = 0;
  slideshowEl.addEventListener('mouseenter', pause, false);
  slideshowEl.addEventListener('focusin', pause, false);
  slideshowEl.addEventListener('mouseleave', play, false);
  slideshowEl.addEventListener('focusout', play, false);

  flickity.on('dragStart', () => {
    isPaused = true;
  });
  update();

}
function recipies() {
  let tickerSpeed = 0.5;
  let flickity = null;
  let isPaused = false;
  const slideshowEl = document.querySelector('#recipies');
  const update = () => {
    if (isPaused) return;
    if (flickity.slides) {
      flickity.x = (flickity.x - tickerSpeed) % flickity.slideableWidth;
      flickity.selectedIndex = flickity.dragEndRestingSelect();
      flickity.updateSelectedSlide();
      flickity.settle(flickity.x);
    }
    window.requestAnimationFrame(update);
  };
  const pause = () => {
    isPaused = true;
  };
  const play = () => {
    if (isPaused) {
      isPaused = false;
      window.requestAnimationFrame(update);
    }
  };

  flickity = new Flickity(slideshowEl, {
    autoPlay: false,
    prevNextButtons: false,
    pageDots: false,
    draggable: true,
    wrapAround: true,
    selectedAttraction: 0.015,
    friction: 0.25,
    imagesLoaded: true
  });
  flickity.x = 0;
  slideshowEl.addEventListener('mouseenter', pause, false);
  slideshowEl.addEventListener('focusin', pause, false);
  slideshowEl.addEventListener('mouseleave', play, false);
  slideshowEl.addEventListener('focusout', play, false);

  flickity.on('dragStart', () => {
    isPaused = true;
  });
  update();

}
function topMenuSlider() {

  let tickerSpeed = 1;
  let flickity = null;
  let isPaused = false;
  const slideshowEl = document.querySelector('#topMenuSlider');
  const update = () => {
    if (isPaused) return;
    if (flickity.slides) {
      flickity.x = (flickity.x - tickerSpeed) % flickity.slideableWidth;
      flickity.selectedIndex = flickity.dragEndRestingSelect();
      flickity.updateSelectedSlide();
      flickity.settle(flickity.x);
    }
    window.requestAnimationFrame(update);
  };
  const pause = () => {
    isPaused = true;
  };
  const play = () => {
    if (isPaused) {
      isPaused = false;
      window.requestAnimationFrame(update);
    }
  };

  flickity = new Flickity(slideshowEl, {
    autoPlay: false,
    prevNextButtons: false,
    pageDots: false,
    draggable: true,
    wrapAround: true,
    selectedAttraction: 0.015,
    friction: 0.25,
    rightToLeft: false,
    cellAlign: 'left',
    dragThreshold: 15,
    imagesLoaded: true
  });
  flickity.x = 0;
  slideshowEl.addEventListener('mouseenter', pause, false);
  slideshowEl.addEventListener('focusin', pause, false);
  slideshowEl.addEventListener('mouseleave', play, false);
  slideshowEl.addEventListener('focusout', play, false);

  flickity.on('dragStart', () => {
    isPaused = true;
  });
  update();

}
if ($("#our-heors")[0]) {
  heroSlider();
};
if ($("#people-saying")[0]) {
  peopleSaying();
}
if ($("#recipies")[0]) {
  recipies();
}

if ($(".my-paroller")[0]) {
  $("[data-paroller-factor]").paroller();

}
if ($(".wow")[0]) {
  wow = new WOW({
    boxClass: 'wow',      // default
    animateClass: 'animated', // default
    offset: 0,          // default
    mobile: false,       // default
    live: true        // default
  })
  wow.init();
}

if ($("#topMenuSlider")[0]) {
  topMenuSlider();
}
var $topPanelHeight;


$('.clickMe').click(function (e) {
  e.preventDefault();
  if ($('body').hasClass('open')) {
    $('body').removeClass('open');
  }

  $('body').toggleClass('topMenuOpen');
  //$('.top-sliding-menu').slideToggle();

  $('.top-sliding-menu').toggleClass('topOpend');

  var $topPanelHeight = $('#topMenuSlider').outerHeight();

  if ($('body').hasClass('topMenuOpen')) {
    if ($(window).width() > 767) {
      $('.top-sliding-menu').css({ 'height': $topPanelHeight + 70 });
      //$('header').css({ 'transform': 'translateY(' + $topPanelHeight + 'px)' });
    }
    else {
      $('.top-sliding-menu').css({ 'height': 'calc(100vh - 90px)' });
    }
  }
  else {
    $('.top-sliding-menu').css({ 'height': '0px' });
    //$('header').css({ 'transform': 'translateY(0px)' });
  }
});
$(window).resize(function () {
  if ($(window).width() > 767) {
    setTimeout(function () {
      var $topPanelHeight = $('#topMenuSlider').outerHeight();

      if ($('body').hasClass('topMenuOpen')) {
        $('.top-sliding-menu').css({ 'height': $topPanelHeight + 70 });
      }
    }, 200);
  }
});

$('#closeTopMenu').click(function (e) {
  e.preventDefault();
  $('.clickMe').trigger('click');
});

$('.insight-img figure').click(function () {
  var $imgFigure = $(this);
  //console.log($imgFigure);
  if ($($imgFigure).hasClass('last-img')) {
    $('.insight-img figure').removeClass('hide-img rotate-img');
    $($imgFigure).siblings('figure').addClass('show-img');
    $('.insight-img figure').fadeIn();
  }
  else {
    $('.insight-img figure').removeClass('show-img rotate-img');
    $($imgFigure).addClass('hide-img');
    $($imgFigure).next('figure').addClass('rotate-img');
    $($imgFigure).fadeOut();
  }
});




$(window).scroll(() => {
  var sidebarHeight = $('.fixedScroller').outerHeight() + 200;
  topOfFooter = $('footer,.discover-prdct').position().top;
  scrollDistanceFromTopOfDoc = $(document).scrollTop() + sidebarHeight;
  scrollDistanceFromTopOfFooter = scrollDistanceFromTopOfDoc - topOfFooter;
  if (scrollDistanceFromTopOfDoc > topOfFooter) {
    $('.fixedScroller').css('margin-top', 0 - scrollDistanceFromTopOfFooter);
  } else {
    $('.fixedScroller').css('margin-top', 0);
  }
});

$(document).ready(function () {
  for (var i = 1; i <= 80; i++) {
    $('.coin-edge').append('<div></div>');
  }
});




function copyToClipboard(el) {

  // resolve the element
  el = (typeof el === 'string') ? document.querySelector(el) : el;

  // handle iOS as a special case
  if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {

    // save current contentEditable/readOnly status
    var editable = el.contentEditable;
    var readOnly = el.readOnly;

    // convert to editable with readonly to stop iOS keyboard opening
    el.contentEditable = true;
    el.readOnly = true;

    // create a selectable range
    var range = document.createRange();
    range.selectNodeContents(el);

    // select the range
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    el.setSelectionRange(0, 999999);

    // restore contentEditable/readOnly to original state
    el.contentEditable = editable;
    el.readOnly = readOnly;
  }
  else {
    el.select();
  }

  // execute copy command
  document.execCommand('copy');
}

$('#linkCopyBtn').click(function () {
  $(this).text('Link Copied');
});
$('.closebtn').click(function (e) {
  e.preventDefault();
  $('.sharePop').fadeOut('slow');
  $('.share-video').fadeIn('fast');
});

$('.share-video').click(function (e) {
  e.preventDefault();
  $(this).fadeOut('fast');
  $('.sharePop').css("display", "flex").hide().fadeIn();
})

if ($(".banner")[0]) {
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: ".banner",
      start: "top 100px",
      endTrigger: ".meet-our-heros",
      end: "top 600px",
      //pin: true,
      scrub: 1,
      //markers: true
    }
  }).to("#bs-1", {
    x: -310,
    y: -20,
    rotate: 50,
  })
    .to("#bs-2", {
      y: -150,
      rotate: -40,

    }, "<")
    .to("#bs-3", {
      y: -150,
      rotate: 60,

    }, "<")
    .to("#bs-4", {
      y: -150,
      rotate: -70,

    }, "<")
    .to("#bs-5", {
      y: -150,
      rotate: 80,

    }, "<")
    .to("#bs-6", {
      y: -150,
      rotate: -20,

    }, "<")
    .to("#bs-7", {
      x: -150,
      rotate: -50,

    }, "<")
    .to("#bs-8", {
      x: -210,
      y: -130,
      rotate: 40,

    }, "<")
    .to("#bs-9", {
      y: -130,
      rotate: -60,

    }, "<")
    .to("#bs-10", {
      x: -110,
      y: -120,
      rotate: 50,

    }, "<")
    .to("#bs-11", {
      y: -110,
      rotate: -180,

    }, "<")
    .to("#bs-12", {
      y: -90,
      rotate: 50,

    }, "<")
    .to("#bs-13", {
      x: 130,
      rotate: -120,
      y: -50,

    }, "<")
    .to("#bs-14", {
      x: -150,
      rotate: 80,

    }, "<")
    .to("#bs-15", {
      x: 170,
      rotate: -40,

    }, "<")
    .to("#bs-16", {
      x: 150,
      rotate: 50,

    }, "<")
    .to("#bs-17", {
      x: 200,
      y: 70,
      rotate: 50,

    }, "<")
    .to("#bs-18", {
      y: -150,
      x: -80,
      rotate: -50,

    }, "<")
    .to("#bs-19", {
      y: 200,
      x: -100,
      rotate: 50,

    }, "<")
    .to("#bs-20", {
      y: 250,
      rotate: -50,

    }, "<")
    .to("#bs-21", {
      x: 200,
      rotate: 50,

    }, "<")
    .to("#bs-22", {
      x: 10,
      y: 220,
      rotate: -50,

    }, "<")
    .to("#bs-23", {
      x: 60,
      y: -60,
      rotate: -50,

    }, "<")
    .to("#bs-24", {
      x: 200,
      y: 160,
      rotate: -50,

    }, "<")
    .to("#bs-25", {
      x: 20,
      y: 220,
      rotate: 50,

    }, "<")
    .to("#bs-26", {
      y: 240,
      x: -100,
      rotate: -80,

    }, "<")
    .to("#bs-27", {
      x: -150,
      y: 100,
      rotate: -50,

    }, "<")
    .to("#bs-28", {
      x: -150,
      rotate: -50,

    }, "<")
    .to("#bs-29", {
      x: -150,
      rotate: 50,

    }, "<")
    .to("#bs-30", {
      y: 200,
      rotate: 50,

    }, "<")
    .to("#bs-31", {
      x: -150,
      rotate: 50,

    }, "<")
    .to("#bs-32", {
      x: -150,
      rotate: -180,

    }, "<");

}

if ($(".insight-img")[0]) {
  function mouseMover() {
    if ($(".cursor").hasClass('active')) {
      gsap.set(".cursor", { xPercent: -50, yPercent: -50 });

      const ball = document.querySelector(".cursor");
      const pos = { x: window.innerWidth / 2, y: window.innerHeight / 2 };
      const mouse = { x: pos.x, y: pos.y };
      const speed = 0.35;

      const xSet = gsap.quickSetter(ball, "x", "px");
      const ySet = gsap.quickSetter(ball, "y", "px");

      window.addEventListener("mousemove", e => {
        mouse.x = e.x;
        mouse.y = e.y;
      });

      gsap.ticker.add(() => {
        var speedX = 0.1;
        var speedY = 0.3;
        // adjust speed for higher refresh monitors
        const dt = 1.0 - Math.pow(1.0 - speed, gsap.ticker.deltaRatio());

        pos.x += (mouse.x - pos.x) * speedX;
        pos.y += (mouse.y - pos.y) * speedY;
        xSet(pos.x);
        ySet(pos.y);
      });
    }
  }


  $('.insight-img').mouseenter(function () {
    $(".cursor").addClass('active');
    mouseMover();
  })
  $('.insight-img').mouseleave(function () {
    $(".cursor").removeClass('active');
    mouseMover();
  })

}

$(function(){
  $('.mobile-top-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    margin: 0,
    dots: false,
    autoplay: false,
    autoplayTimeout: 2000,
    autoplaySpeed: 3000,
    autoplayHoverPause: false,
  });

  $('.owl-carousel').data('owl.carousel').difference = function (first, second) {
	return {
		x: first.x - second.x + (first.y - second.y),
		y: first.y - second.y
	};
};
});



$(".btnPopOpening").click(function () {
  $('.modal-content > .row').remove();
  var popContents = $(this).closest('.item-box').children('.popContentWrap').html();
  var $html = $(popContents);
  $html.find(".tab-pane").each(function(){
    var $tabPane = $(this);
    $tabPane.attr("id", $tabPane.attr("id") + "Op");
  })
  $html.find("a[data-toggle='pill']").each(function(){
    var $anchor = $(this);
    $anchor.attr("href", $anchor.attr("href") + "Op");
  })
  $('.modal-content').append($html);
  $('.tabPopSwitch a').on('click', function (e) {
  e.preventDefault()
  $(this).tab('show')
})
});

function popUpoverFlow() {
  var $winHeight, $popHeight, $popTitileHeight, $popTabNavHeight;
   $winHeight = $(window).height();
var exampleModal = document.getElementById('reci-pop')
  exampleModal.addEventListener('shown.bs.modal', function (event) {
    setTimeout(function () {
      $popHeight = $('#reci-pop').outerHeight();
      $popTitileHeight = $('#reci-pop h5').outerHeight();
      $popTabNavHeight = $('#reci-pop .nav-pills').outerHeight();
      var plusItems = $popTitileHeight + $popTabNavHeight + 200;
      var maxHeightTab =  $winHeight - plusItems;
      $(".recipe-popup .tab-pane").css({ "max-height": maxHeightTab, "overflow": "auto" });
    }, 150);
  });
}
if ($("#reci-pop")[0]) {
  popUpoverFlow();
};
$(window).resize(function () {
  popUpoverFlow();
  if ($("body.modal-open")[0]) {
    var $winHeight, $popHeight, $popTitileHeight, $popTabNavHeight;
    $winHeight = $(window).height();
    $popHeight = $('#reci-pop').outerHeight();
    $popTitileHeight = $('#reci-pop h5').outerHeight();
    $popTabNavHeight = $('#reci-pop .nav-pills').outerHeight();
    var plusItems = $popTitileHeight + $popTabNavHeight + 200;
    var maxHeightTab = $winHeight - plusItems;
    $(".recipe-popup .tab-pane").css({ "max-height": maxHeightTab, "overflow": "auto" });
  }
});

if ($(".masonry-grid")[0]) {

  var $grid = $('.masonry-grid').imagesLoaded( function() {
  // init Masonry after all images have loaded
  $grid.masonry({
    itemSelector: '.people-box'
  });
});
};

if( /iPhone/i.test(navigator.userAgent) ) {
 document.querySelector('html').classList.add('iphone');
}